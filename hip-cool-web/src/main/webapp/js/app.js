var app=angular.module("HipAndCool", []);
var landingURL = window.location.protocol + "//" + window.location.hostname + ":" +
    window.location.port + window.location.pathname;

app.controller("LandingController",['$scope', '$http', function ($scope, $http) {

    $scope.loggedOn = false;

    $(document).ready(function () {
        $('#logo').addClass('animated fadeInDown');
        $("input:text:visible:first").focus();
    });
    $('#username').focus(function() {
        $('label[for="username"]').addClass('selected');
    });
    $('#username').blur(function() {
        $('label[for="username"]').removeClass('selected');
    });
    $('#password').focus(function() {
        $('label[for="password"]').addClass('selected');
    });
    $('#password').blur(function() {
        $('label[for="password"]').removeClass('selected');
    });

    //List of URLs to call Restful services
    var propertiesURL = landingURL + "rest/applicationpropertyrest/properties";

    $scope.login = function(username, password) {
        $http.get(landingURL + "rest/login/" + username + '/' + password).then(function (response) {
            $scope.loggedOn = response.data;
            if($scope.loggedOn){
                //Set the application properties, retrieved from the rest call, in the scope
                $http.get(propertiesURL).then(function (response) {
                    $scope.properties = response.data;
                });
            }
        });
    };

    $scope.cancel = function() {
        window.history.back()
    };

    $scope.register = function() {
        if ($scope.reg_username === null || $scope.reg_username === "") {
            alert("Username can't be blank");
            return false;
        }
        else if($scope.reg_password.length<6) {
            alert("Password must be at least 6 characters long.");
            return false;
        }
        else if ($scope.reg_password !== $scope.repeat_psw) {
            alert("Confirm Password should match with the Password");
            return false;
        } else{
            if(landingURL.indexOf("register") !== -1){
                landingURL = landingURL.replace("register", "");
            }

            $http.get(landingURL + "rest/login/signup/" + $scope.reg_username + '/' + $scope.reg_password).then(function (response) {
                if(response.data){
                    $scope.login($scope.reg_username, $scope.reg_password);
                }
            })
        }
    };
}]);