package selfimprovement.co.za.util;

import selfimprovement.co.za.services.impl.ApplicationPropertyRestImpl;
import selfimprovement.co.za.services.impl.LoginRestImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("rest")
public class ApplicationRestResources extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<Class<?>>();
        addResourceClasses(resources);
        return resources;
    }

    private void addResourceClasses(Set<Class<?>> resources) {
        resources.add(ApplicationPropertyRestImpl.class);
        resources.add(LoginRestImpl.class);
    }
}
