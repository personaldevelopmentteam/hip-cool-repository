package selfimprovement.co.za.util;

public class ApplicationConstants {

    public static final String SSH_USER_USERNAME = "ssh.user.username";
    public static final String SSH_USER_PASSWORD = "ssh.user.password";
    public static final String SSH_CONNECTION_HOST = "ssh.connection.host";
    public static final String SSH_CONNECTION_PORT = "ssh.connection.port";
}
