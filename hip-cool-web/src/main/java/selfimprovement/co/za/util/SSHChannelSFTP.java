package selfimprovement.co.za.util;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class SSHChannelSFTP {
    private static final Logger logger = LoggerFactory.getLogger(SSHChannelSFTP.class);

    @Inject
    private ApplicationProperties properties;

    private Session session;

    private ChannelSftp channelSftp;

    @PostConstruct
    private void constructSampleChannelSFTP() {
        try {
            connectSession();
            connectChannelSFTP();
        } catch (JSchException e) {
            logger.error("Could not connect to channel SFTP.", e);
        }
    }

    @PreDestroy
    private void destroySampleChannelSFTP(){
        logger.info("Disconnecting from SFTP Channel...");
        channelSftp.disconnect();
        logger.info("SFTP Channel disconnected.");
        logger.info("Disconnecting from session...");
        session.disconnect();
        logger.info("Disconnected from session.");
    }

    /**
     * This method establishes a session connection to ssh machine using credentials found in the
     * application.properties file.
     * @throws JSchException if not able to connect.
     */
    private void connectSession() throws JSchException {
        logger.info("Establishing Connection to session...");
        JSch jsch = new JSch();
        session = jsch.getSession(
                properties.getProperty(ApplicationConstants.SSH_USER_USERNAME),
                properties.getProperty(ApplicationConstants.SSH_CONNECTION_HOST),
                Integer.parseInt(properties.getProperty(ApplicationConstants.SSH_CONNECTION_PORT)));
        session.setPassword(properties.getProperty(ApplicationConstants.SSH_USER_PASSWORD));
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        logger.info("Connection to session established.");
    }

    /**
     * This method creates a SFTP Channel.
     * @throws JSchException when not able to connect.
     */
    private void connectChannelSFTP() throws JSchException {
        logger.info("Creating SFTPChannel.");
        channelSftp = (ChannelSftp) session.openChannel("sftp");
        channelSftp.connect();
        logger.info("SFTP Channel created.");
    }

    public Session getSession() {
        return session;
    }

    public ChannelSftp getChannelSftp() {
        return channelSftp;
    }
}
