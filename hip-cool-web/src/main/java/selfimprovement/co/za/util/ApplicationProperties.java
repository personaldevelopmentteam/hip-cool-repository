package selfimprovement.co.za.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Singleton;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Singleton
public class ApplicationProperties {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);

    private Properties properties;

    public ApplicationProperties() {
    }

    @PostConstruct
    public void initializePropertiesPostConstruct(){
        logger.info("Initializing application properties.");
        properties = new Properties();
        try {
            properties.putAll(loadProperties("application.properties"));
        } catch (IOException e) {
            logger.error("Could not load application properties from application.properties.",e);
        }
    }

    @PreDestroy
    private void destroyPropertyValueRetriever() {
        logger.info("Destroying ApplicationProperties.");
    }

    /**
     * This method loads all the properties specified in the file.
     * @param propertiesFileName - The name of the file i.e. "application.properties"
     * @return A Property list.
     * @throws IOException i.e. unable to the stream resource.
     */
    private Properties loadProperties(String propertiesFileName) throws IOException {
        properties = new Properties();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

        if (inputStream != null) {
            properties.load(inputStream);
        } else {
            throw new FileNotFoundException("Property file '" + propertiesFileName + "'not found in the classpath");
        }

        return properties;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public void addProperties(Properties properties) {
        this.properties.putAll(properties);
    }

    public void addProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public Properties getProperties() {
        return this.properties;
    }

}