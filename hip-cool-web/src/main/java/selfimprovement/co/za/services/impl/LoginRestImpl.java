package selfimprovement.co.za.services.impl;

import selfimprovement.co.za.dao.HacLoginDao;
import selfimprovement.co.za.services.LoginRest;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.Path;
import javax.ws.rs.core.SecurityContext;

/**
 * Created by Juan on 2018/01/08.
 */
@Path("/login")
@Named
@RequestScoped
public class LoginRestImpl implements LoginRest {

    @PersistenceContext(unitName = "selfimprovement.hac.domain")
    private EntityManager em;

    @Resource
    private UserTransaction userTransaction;

    private HacLoginDao hacLoginDao;

    @PostConstruct
    private void constructLoginRestImpl(){
        this.hacLoginDao = new HacLoginDao(em, userTransaction);
    }

    public Boolean login(String username, String password, SecurityContext context) {
        return hacLoginDao.canLogin(username,password);
    }

    public Boolean register(String username, String password, SecurityContext context) {
        return hacLoginDao.register(username,password);
    }
}
