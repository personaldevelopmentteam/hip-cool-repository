package selfimprovement.co.za.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;

public interface ApplicationPropertyRest {

    /** This is a sample method to use this class. This method retrieves the application properties from the
     * application.properties file.
     * @param context - The security context (optional parameter)
     * @return An entry set of application properties.
     */
    @GET
    @Path("/properties")
    @Produces({"application/json"})
    Map<String, String> retrieveApplicationProperties(@Context SecurityContext context);
}
