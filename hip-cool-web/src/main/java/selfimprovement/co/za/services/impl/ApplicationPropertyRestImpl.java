package selfimprovement.co.za.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import selfimprovement.co.za.services.ApplicationPropertyRest;
import selfimprovement.co.za.util.ApplicationProperties;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.util.HashMap;
import java.util.Map;

@Path("/applicationpropertyrest")
public class ApplicationPropertyRestImpl implements ApplicationPropertyRest {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationPropertyRestImpl.class);

    @Inject
    private ApplicationProperties properties;

    /** This is a sample method to use this class. This method retrieves the application properties from the
     * application.properties file.
     * @param context - The security context (optional parameter)
     * @return A HashMap of application properties.
     */
    public Map<String, String> retrieveApplicationProperties(@Context SecurityContext context) {
        logger.info("Retrieving application properties.");

        Map<String, String> mapFromSet = new HashMap<String, String>();
        for(Map.Entry<Object, Object> entry : properties.getProperties().entrySet())
        {
            mapFromSet.put(entry.getKey().toString(), entry.getValue().toString());
        }

        return mapFromSet;
    }
}
