package selfimprovement.co.za.services;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 * Created by Juan on 2018/01/08.
 */
public interface LoginRest {

    /**
     * This method is used to log a person into the app
     * @param username of the person's account
     * @param password of the person's account
     * @param context security context such as version etc.
     * @return a boolean value of the details existence result
     */
    @GET
    @Path("/{username}/{password}")
    @Produces({"application/json"})
    Boolean login(@PathParam("username") String username, @PathParam("password") String password,
                  @Context SecurityContext context);

    @GET
    @Path("/signup/{username}/{password}")
    Boolean register(@PathParam("username") String username, @PathParam("password") String password,
                  @Context SecurityContext context);
}
