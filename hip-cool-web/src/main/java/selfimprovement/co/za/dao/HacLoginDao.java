package selfimprovement.co.za.dao;

import selfimprovement.co.za.model.HacLoginEntity;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.*;

/**
 * Created by Juan on 2018/01/08.
 */
public class HacLoginDao {

    protected EntityManager em;
    protected UserTransaction userTransaction;

    public HacLoginDao(EntityManager em, UserTransaction userTransaction){
        this.em = em;
        this.userTransaction = userTransaction;
    }

    public boolean canLogin(String username, String password){
        Query query = em.createQuery("SELECT hac FROM HacLoginEntity hac " +
                                        "WHERE hac.userName = :username " +
                                        "AND hac.password = :password");
        query.setParameter("username", username);
        query.setParameter("password", password);

        return !query.getResultList().isEmpty();
    }

    public Boolean register(String username, String password){
        HacLoginEntity hacLoginEntity = new HacLoginEntity();
        hacLoginEntity.setUserName(username);
        hacLoginEntity.setPassword(password);
        try {
            userTransaction.begin();
            em.persist(hacLoginEntity);
            userTransaction.commit();
        } catch (NotSupportedException e) {
            e.printStackTrace();
        } catch (SystemException e) {
            e.printStackTrace();
        } catch (HeuristicMixedException e) {
            e.printStackTrace();
        } catch (HeuristicRollbackException e) {
            e.printStackTrace();
        } catch (RollbackException e) {
            e.printStackTrace();
        }

        return Boolean.TRUE;
    }
}
