import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import selfimprovement.co.za.util.ApplicationProperties;

import java.io.IOException;
import java.io.PrintStream;

public class ApplicationPropertiesTest {

    @InjectMocks
    private ApplicationProperties properties;

    @Before
    public void setupApplicationPropertiesTest() throws IOException {
        System.out.println("Setting up Application Properties Tests.");
        MockitoAnnotations.initMocks(this);
        this.properties.initializePropertiesPostConstruct();
    }

    @Test
    public void printApplicationPropertiesTest(){
        PrintStream writer = new PrintStream(System.out);
        Assert.assertNotNull(properties);
        properties.getProperties().list(writer);
    }

}
